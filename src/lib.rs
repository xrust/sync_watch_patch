#![feature(unboxed_closures)]

mod data;
mod notify;
mod sync;
mod utils;
mod walk;
mod watch;

use ctor::ctor;
use data::{EditEntry, Editor, Priority, RenameEntry, Syncer};
use path_absolutize::Absolutize;
use procspawn;
use serde::{Deserialize, Serialize};
use serde_traitobject as st;
use std::time::SystemTime;
use std::{
    collections::BTreeMap,
    env::current_dir,
    ops::{AddAssign, Fn},
    path::PathBuf,
    string::ToString,
};
use walk::walk;
use watch::watch;

#[ctor]
fn init() {
    procspawn::init();
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SyncWatch {
    priority: Priority,
    src_dir: PathBuf,
    dest_dir: PathBuf,
    include: Vec<String>,
    exclude: Vec<String>,
    ignore: Vec<String>,
    editor: Editor,
}

impl AddAssign for SyncWatch {
    fn add_assign(&mut self, other: Self) {
        self.exclude.extend(other.exclude);
        self.editor.edit.extend(other.editor.edit);
        self.editor.edit_once.extend(other.editor.edit_once);
        self.editor.rename.extend(other.editor.rename);
    }
}

impl SyncWatch {
    pub fn new<P: Into<PathBuf>>(src_dir: P, dest_dir: P) -> Self {
        let src_dir: PathBuf = src_dir.into();
        let dest_dir: PathBuf = dest_dir.into();
        let ignore = dest_dir.to_str().unwrap().to_string();
        let src_dir = if src_dir.is_absolute() {
            src_dir
        } else {
            src_dir
                .absolutize_from(&current_dir().unwrap())
                .unwrap()
                .to_path_buf()
        };
        let dest_dir = if dest_dir.is_absolute() {
            dest_dir
        } else {
            dest_dir
                .absolutize_from(&current_dir().unwrap())
                .unwrap()
                .to_path_buf()
        };
        Self {
            priority: 0,
            src_dir,
            dest_dir,
            include: vec![],
            exclude: vec![],
            ignore: vec![ignore],
            editor: Editor {
                edit: BTreeMap::new(),
                edit_once: BTreeMap::new(),
                rename: BTreeMap::new(),
            },
        }
    }

    pub fn priority<'a>(&'a mut self, priority: u8) -> &'a mut Self {
        self.priority = priority;
        self
    }

    pub fn include<'a>(&'a mut self, include: &[impl ToString]) -> &'a mut Self {
        self.include = include.iter().map(|e| e.to_string()).collect();
        self
    }

    pub fn exclude<'a>(&'a mut self, exclude: &[impl ToString]) -> &'a mut Self {
        self.exclude = exclude.iter().map(|e| e.to_string()).collect();
        self
    }

    pub fn ignore<'a>(&'a mut self, ignore: &[impl ToString]) -> &'a mut Self {
        for i in ignore {
            self.ignore.push(i.to_string());
        }
        self
    }

    pub fn edit<'a, F>(
        &'a mut self,
        tag: impl ToString,
        glob: impl ToString,
        fun: F,
    ) -> &'a mut Self
    where
        F: Fn(String) -> String + Serialize + for<'de> Deserialize<'de> + Send + Sync + 'static,
    {
        let fun: st::Arc<dyn st::Fn(String) -> String + Send + Sync> = st::Arc::new(fun);
        let entry = EditEntry {
            glob: glob.to_string(),
            fun,
            modified: SystemTime::now(),
        };
        self.editor
            .edit
            .insert((self.priority, tag.to_string()), entry);
        self
    }

    pub fn edit_once<'a, F>(
        &'a mut self,
        tag: impl ToString,
        glob: impl ToString,
        fun: F,
    ) -> &'a mut Self
    where
        F: Fn(String) -> String + Serialize + for<'de> Deserialize<'de> + Send + Sync + 'static,
    {
        let fun: st::Arc<dyn st::Fn(String) -> String + Send + Sync> = st::Arc::new(fun);
        let entry = EditEntry {
            glob: glob.to_string(),
            fun,
            modified: SystemTime::now(),
        };
        self.editor
            .edit_once
            .insert((self.priority, tag.to_string()), entry);
        self
    }

    pub fn edit_all<'a, F>(&'a mut self, tag: impl ToString, fun: F) -> &'a mut Self
    where
        F: Fn(String) -> String + Serialize + for<'de> Deserialize<'de> + Send + Sync + 'static,
    {
        self.edit(tag, "**/*", fun)
    }

    pub fn rename<'a, F>(
        &'a mut self,
        tag: impl ToString,
        glob: impl ToString,
        fun: F,
    ) -> &'a mut Self
    where
        F: Fn(PathBuf) -> PathBuf + Serialize + for<'de> Deserialize<'de> + Send + Sync + 'static,
    {
        let fun: st::Arc<dyn st::Fn(PathBuf) -> PathBuf + Send + Sync> = st::Arc::new(fun);
        let entry = RenameEntry {
            glob: glob.to_string(),
            fun,
            modified: SystemTime::now(),
        };
        self.editor
            .rename
            .insert((self.priority, tag.to_string()), entry);
        self
    }

    pub fn rename_all<'a, F>(&'a mut self, tag: impl ToString, fun: F) -> &'a mut Self
    where
        F: Fn(PathBuf) -> PathBuf + Serialize + for<'de> Deserialize<'de> + Send + Sync + 'static,
    {
        self.rename(tag, "**/*", fun)
    }

    pub fn spawn<'a>(&'a mut self) -> &'a mut Self {
        let Self {
            src_dir,
            dest_dir,
            include,
            exclude,
            ignore,
            editor,
            ..
        } = self;
        Syncer::kill(src_dir, dest_dir);
        walk(editor, src_dir, dest_dir, include, exclude, ignore);
        let data = (
            src_dir.clone(),
            dest_dir.clone(),
            include.clone(),
            exclude.clone(),
            ignore.clone(),
            editor.clone(),
        );
        procspawn::spawn(data, |data| {
            let (src_dir, dest_dir, include, exclude, ignore, editor) = data;
            watch(&src_dir, &dest_dir, &include, &exclude, &ignore, &editor);
        });
        self
    }

    pub fn kill<'a>(&'a mut self) -> &'a mut Self {
        Syncer::kill(&self.src_dir, &self.dest_dir);
        self
    }
}
