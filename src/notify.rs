use crate::notify::Action::*;
use notify::{
    event::{CreateKind, DataChange, ModifyKind, RemoveKind, RenameMode},
    Event,
    EventKind::{Access, Any, Create, Modify, Other, Remove},
};

pub(crate) enum Action {
    CreateOrModifyFiles(Vec<PathBuf>),
    RemoveFiles(Vec<PathBuf>),
    CreateDirs(Vec<PathBuf>),
    RemoveDirs(Vec<PathBuf>),
    Todo(Event),
}
use std::path::PathBuf;

pub(crate) fn event_to_action(event: Result<Event, notify::Error>) -> Option<Action> {
    let event = event.unwrap();
    match event.clone() {
        Event {
            kind: Modify(ModifyKind::Data(DataChange::Content)),
            paths,
            ..
        } => Some(CreateOrModifyFiles(paths)),
        Event {
            kind: Create(CreateKind::File),
            paths,
            ..
        } => Some(CreateOrModifyFiles(paths)),
        Event {
            kind: Create(CreateKind::Folder),
            paths,
            ..
        } => Some(CreateDirs(paths)),
        Event {
            kind: Remove(RemoveKind::File),
            paths,
            ..
        } => Some(RemoveFiles(paths)),
        Event {
            kind: Remove(RemoveKind::Folder),
            paths,
            ..
        } => Some(RemoveDirs(paths)),
        Event {
            kind: Modify(ModifyKind::Metadata(_)),
            ..
        } => None, // It is not possible to sync metadata changes.
        Event {
            kind: Modify(ModifyKind::Name(RenameMode::Any)),
            ..
        } => None, // Ignore rename events.
        Event {
            kind: Access(_), ..
        } => None, // Ignore non mutating events.
        Event {
            kind: Any | Other, ..
        } => None, // Ignore unknown and meta events.
        Event {
            kind: Create(CreateKind::Other | CreateKind::Any),
            ..
        } => Some(Todo(event)),
        Event {
            kind: Modify(_) | Remove(_),
            ..
        } => Some(Todo(event)),
    }
}
