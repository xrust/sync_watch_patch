use crate::data::Storage;
use crate::data::Worker;
use disk_persist::DiskPersist;
use std::path::PathBuf;

#[allow(dead_code)]
pub(crate) fn add_worker(src_dir: PathBuf, dest_dir: PathBuf, worker: Worker) {
    let persist: DiskPersist<Storage> = DiskPersist::init("storage").unwrap();
    let mut storage = persist.read().unwrap().unwrap();
    let key = (src_dir, dest_dir);
    let mut syncer = storage.syncers.get(&key).unwrap().clone();
    syncer.workers.push(worker);
    storage.syncers.insert(key, syncer.clone());
    persist.write(&storage).unwrap();
}

#[allow(dead_code)]
pub(crate) fn get_workers(src_dir: PathBuf, dest_dir: PathBuf) -> Vec<Worker> {
    let persist: DiskPersist<Storage> = DiskPersist::init("storage").unwrap();
    let storage = persist.read().unwrap().unwrap();
    let syncer = storage.syncers.get(&(src_dir, dest_dir)).unwrap().clone();
    syncer.workers
}
