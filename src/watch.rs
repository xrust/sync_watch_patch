use crate::data::{Editor, Syncer};
use crate::notify::{event_to_action, Action::*};
use crate::sync::{get_dest, sync};
use colored::Colorize;
use notify::{Config, Event, RecommendedWatcher, RecursiveMode, Watcher};
use std::fs::{create_dir, remove_dir, remove_file};
use std::io::ErrorKind::{AlreadyExists, NotFound};
use std::path::Path;
use std::process;
use std::sync::mpsc::channel;
use wax::{Any, Glob, Pattern};

pub(crate) fn watch(
    src_dir: &Path,
    dest_dir: &Path,
    include: &Vec<String>,
    exclude: &Vec<String>,
    ignore: &Vec<String>,
    editor: &Editor,
) {
    let e: Vec<Glob> = exclude.iter().map(|e| Glob::new(e).unwrap()).collect();
    let exc: Any = wax::any::<Glob, _>(e).unwrap();

    let i: Vec<Glob> = include.iter().map(|i| Glob::new(i).unwrap()).collect();
    let inc: Any = wax::any::<Glob, _>(i).unwrap();

    let ig: Vec<Glob> = ignore.iter().map(|i| Glob::new(i).unwrap()).collect();
    let ign: Any = wax::any::<Glob, _>(ig).unwrap();

    Syncer::save(process::id(), src_dir, dest_dir);
    let (tx, rx) = channel();
    let mut watcher = RecommendedWatcher::new(tx, Config::default()).unwrap();
    watcher.watch(src_dir, RecursiveMode::Recursive).unwrap();
    for e in rx {
        let action = if let Some(action) = event_to_action(e) {
            action
        } else {
            continue;
        };
        match action {
            CreateOrModifyFiles(paths) => {
                for src in paths {
                    let rel_src = src.strip_prefix(src_dir).unwrap();
                    if !ign.is_match(rel_src) && (!exc.is_match(rel_src) || inc.is_match(rel_src)) {
                        let dest = get_dest(editor, dest_dir, &rel_src);
                        sync(editor, &src, &dest, &rel_src);
                    }
                }
            }
            RemoveFiles(paths) => {
                for src in paths {
                    let rel_src = src.strip_prefix(src_dir).unwrap();
                    if !ign.is_match(rel_src) && (!exc.is_match(rel_src) || inc.is_match(rel_src)) {
                        let dest = get_dest(editor, dest_dir, &rel_src);
                        if let Err(e) = remove_file(dest) {
                            if e.kind() != NotFound {
                                Err(e).unwrap()
                            }
                        };
                    }
                }
            }
            CreateDirs(paths) => {
                for src in paths {
                    let rel_src = src.strip_prefix(src_dir).unwrap();
                    if !ign.is_match(rel_src) && (!exc.is_match(rel_src) || inc.is_match(rel_src)) {
                        let dest = get_dest(editor, dest_dir, &rel_src);
                        if let Err(e) = create_dir(&dest) {
                            if e.kind() != AlreadyExists {
                                Err(e).unwrap()
                            }
                        };
                    }
                }
            }
            RemoveDirs(paths) => {
                for src in paths {
                    let rel_src = src.strip_prefix(src_dir).unwrap();
                    if !ign.is_match(rel_src) && (!exc.is_match(rel_src) || inc.is_match(rel_src)) {
                        let dest = get_dest(editor, dest_dir, &rel_src);
                        if let Err(e) = remove_dir(dest) {
                            if e.kind() != NotFound {
                                Err(e).unwrap()
                            }
                        };
                    }
                }
            }
            Todo(event) => todo(&event, src_dir, &inc, &exc, &ign),
        }
    }
}

fn todo(event: &Event, src_dir: &Path, inc: &Any, exc: &Any, ign: &Any) {
    for src in &event.paths {
        let rel_src = src.strip_prefix(src_dir).unwrap();
        // TODO: calculate is_dir
        if !ign.is_match(rel_src) && (!exc.is_match(rel_src) || inc.is_match(rel_src)) {
            println!("{}", format!("{event:?}").red())
        }
    }
}
