use crate::{
    data::Editor,
    sync::{get_dest, sync},
};
use ignore::WalkBuilder;
use ignore::WalkState::Continue;
use std::{fs::create_dir, io::ErrorKind::AlreadyExists};
use std::{fs::metadata, path::Path};
use wax::{Any, Glob, Pattern};

pub(crate) fn walk(
    editor: &Editor,
    src_dir: &Path,
    dest_dir: &Path,
    include: &Vec<String>,
    exclude: &Vec<String>,
    ignore: &Vec<String>,
) {
    let e: Vec<Glob> = exclude.iter().map(|e| Glob::new(e).unwrap()).collect();
    let exc: Any = wax::any::<Glob, _>(e).unwrap();

    let i: Vec<Glob> = include.iter().map(|i| Glob::new(i).unwrap()).collect();
    let inc: Any = wax::any::<Glob, _>(i).unwrap();

    let ig: Vec<Glob> = ignore.iter().map(|i| Glob::new(i).unwrap()).collect();
    let ign: Any = wax::any::<Glob, _>(ig).unwrap();

    let walker = WalkBuilder::new(src_dir)
        .standard_filters(false)
        .threads(num_cpus::get())
        .build_parallel();
    walker.run(|| {
        Box::new(|entry| {
            let entry = entry.unwrap();
            let src = entry.path();
            let rel_src = src.strip_prefix(src_dir).unwrap();
            if ign.is_match(rel_src) || (exc.is_match(rel_src) && !inc.is_match(rel_src)) {
                return Continue;
            }

            let dest = get_dest(editor, dest_dir, &rel_src);
            let src_meta = metadata(&src).unwrap();

            if src_meta.is_dir() {
                if let Err(e) = create_dir(&dest) {
                    if e.kind() != AlreadyExists {
                        Err(e).unwrap()
                    }
                };
            } else {
                sync(editor, &src, &dest, &rel_src);
            }
            Continue
        })
    });
}
