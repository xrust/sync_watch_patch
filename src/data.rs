use disk_persist::DiskPersist;
use serde::{Deserialize, Serialize};
use serde_traitobject as st;
use std::path::Path;
use std::time::SystemTime;
use std::{
    collections::{BTreeMap, HashMap},
    path::PathBuf,
};
use sysinfo::{Pid, PidExt, ProcessExt, System, SystemExt};

pub(crate) type Priority = u8;
pub(crate) type Tag = String;

#[derive(Clone, Serialize, Deserialize)]
pub(crate) struct Storage {
    pub syncers: HashMap<(PathBuf, PathBuf), Syncer>,
}

#[derive(Clone, Serialize, Deserialize)]
pub(crate) struct Syncer {
    pub src_dir: PathBuf,
    pub dest_dir: PathBuf,
    pub pid: u32,
    pub workers: Vec<Worker>,
}

#[derive(Clone, Serialize, Deserialize)]
pub(crate) struct Worker {
    pub pid: u32,
    pub socket_path: PathBuf,
}

#[derive(Serialize, Deserialize, Clone)]
pub(crate) struct Editor {
    pub edit: BTreeMap<(Priority, Tag), EditEntry>,
    pub edit_once: BTreeMap<(Priority, Tag), EditEntry>,
    pub rename: BTreeMap<(Priority, Tag), RenameEntry>,
}

#[derive(Serialize, Deserialize, Clone)]
pub(crate) struct EditEntry {
    pub glob: String,
    pub modified: SystemTime,
    pub fun: st::Arc<dyn st::Fn(String) -> String + Send + Sync>,
}

#[derive(Serialize, Deserialize, Clone)]
pub(crate) struct RenameEntry {
    pub glob: String,
    pub modified: SystemTime,
    pub fun: st::Arc<dyn st::Fn(PathBuf) -> PathBuf + Send + Sync>,
}

impl Syncer {
    pub fn save(pid: u32, src_dir: &Path, dest_dir: &Path) {
        let persist: DiskPersist<Storage> = DiskPersist::init("storage").unwrap();
        let key = (src_dir.to_path_buf(), dest_dir.to_path_buf());
        let syncer = Syncer {
            src_dir: src_dir.to_path_buf(),
            dest_dir: dest_dir.to_path_buf(),
            pid,
            workers: vec![],
        };
        let storage = if let Some(mut storage) = persist.read().unwrap() {
            storage.syncers.insert(key, syncer);
            storage
        } else {
            let mut syncers = HashMap::new();
            syncers.insert(key, syncer);
            Storage { syncers }
        };
        persist.write(&storage).unwrap();
    }
    pub fn kill(src_dir: &Path, dest_dir: &Path) -> bool {
        let persist: DiskPersist<Storage> = DiskPersist::init("storage").unwrap();
        if let Some(Storage { mut syncers }) = persist.read().unwrap() {
            if let Some(syncer) = syncers.remove(&(src_dir.to_path_buf(), dest_dir.to_path_buf())) {
                for worker in syncer.workers {
                    if let Some(process) = System::new_all().process(Pid::from_u32(worker.pid)) {
                        process.kill();
                    }
                }
                if let Some(process) = System::new_all().process(Pid::from_u32(syncer.pid)) {
                    return process.kill();
                }
            }
        }
        false
    }
}

// impl Worker {
//     fn new(pid: u32, socket_path: &Path) -> Self {
//         Self {
//             pid,
//             socket_path: socket_path.to_path_buf(),
//         }
//     }
// }
