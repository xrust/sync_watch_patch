use crate::data::Editor;
use std::{
    fs::{copy, create_dir_all, metadata, read_to_string, File, Metadata},
    io::{ErrorKind::NotFound, Write},
    path::{Path, PathBuf},
};
use wax::{Glob, Pattern};

pub(crate) fn sync(editor: &Editor, src: &Path, dest: &Path, rel_src: &Path) {
    let src_meta = if let Ok(meta) = metadata(&src) {
        meta
    } else {
        return;
    };
    let src_modified = is_src_modified(&src_meta, dest);
    if let Ok(src_data) = read_to_string(&src) {
        let Some((editor_modified, edited_data)) = edit(
            src_data.clone(),
            editor.clone(),
            dest.to_path_buf(),
            src_meta.clone(),
            rel_src.to_path_buf(),
        ) else {
            return;
        };
        if editor_modified || src_modified {
            write(&edited_data, &src_meta, dest);
        }
    } else {
        // binary file
        if !src_modified {
            return;
        }
        create_dir_all(dest.parent().unwrap()).unwrap();
        if let Err(e) = copy(&src, &dest) {
            if e.kind() != NotFound {
                Err(e).unwrap()
            }
        }
    }
}

fn write(data: &str, src_meta: &Metadata, dest: &Path) {
    create_dir_all(dest.parent().unwrap()).unwrap();
    let mut f = File::create(&dest).unwrap();
    f.set_permissions(src_meta.permissions()).unwrap();
    f.write_all(data.as_bytes()).unwrap();
}

fn is_src_modified(src_meta: &Metadata, dest: &Path) -> bool {
    let src_modified = src_meta.modified().unwrap();
    if let Ok(dest_meta) = metadata(&dest) {
        src_modified > dest_meta.modified().unwrap()
    } else {
        true
    }
}

pub(crate) fn get_dest(editor: &Editor, dest_dir: &Path, rel_src: &Path) -> PathBuf {
    let mut dest = dest_dir.to_path_buf();
    dest.push(&rel_src);
    let mut dir_changed = false;
    for (_, entry) in editor.rename.iter() {
        let pattern = Glob::new(&entry.glob).unwrap();
        if !pattern.is_match(rel_src) {
            continue;
        }
        dir_changed = true;
        dest = (entry.fun)(dest.clone());
    }
    if dir_changed {
        create_dir_all(dest.parent().unwrap()).unwrap();
    }
    dest
}

pub(crate) fn edit(
    src_data: String,
    editor: Editor,
    dest: PathBuf,
    src_meta: Metadata,
    rel_src: PathBuf,
) -> Option<(bool, String)> {
    let src_modified = src_meta.modified().unwrap();
    let mut editor_modified = false;
    let mut edited_data = src_data;
    for (_, entry) in editor.edit_once.iter() {
        let pattern = Glob::new(&entry.glob).unwrap();
        if !pattern.is_match(rel_src.as_path()) {
            continue;
        }
        if Path::new(&dest).exists() {
            return None;
        }
        if entry.modified > src_modified {
            editor_modified = true;
        }
        edited_data = (entry.fun)(edited_data.clone());
    }
    for (_, entry) in editor.edit.iter() {
        let pattern = Glob::new(&entry.glob).unwrap();
        if !pattern.is_match(rel_src.as_path()) {
            continue;
        }
        if entry.modified > src_modified {
            editor_modified = true;
        }
        edited_data = (entry.fun)(edited_data.clone());
    }
    Some((editor_modified, edited_data))
}
